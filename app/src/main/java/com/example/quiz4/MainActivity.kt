package com.example.quiz4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.properties.Delegates

class MainActivity : AppCompatActivity() {
    private val items = ArrayList<Model>()
//    private var firstPlayer = true
//    private lateinit var player: Model
    private lateinit var adapter: RecyclerViewAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        nine.setOnClickListener { init(nine) }
        sixteen.setOnClickListener { init(sixteen) }
        twentyFive.setOnClickListener { init(twentyFive) }

    }

    private fun init(button: Button) {
        var spanCount = 0
        if (button.text == "3x3")
            spanCount = 3
        else if(button.text == "4x4")
            spanCount = 4
        else if( button.text == "5x5")
            spanCount = 5


        recyclerView.layoutManager = GridLayoutManager(this, 3)
        recyclerView.adapter = RecyclerViewAdapter(items)
        adapter

    }




}


