package com.example.quiz4

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.board.view.*

private var firstPlayer = true
class RecyclerViewAdapter(private var dataSet: ArrayList<Model>) :
    RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder>() {


    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),View.OnClickListener {
        private lateinit var items:Model
            fun onBind() {



            items = dataSet[adapterPosition]

            if (itemView.isClickable && firstPlayer) {
                itemView.imageButton.setImageResource(R.mipmap.baseline_close_black_18dp)
                firstPlayer = false
                itemView.isClickable = false

            } else
                itemView.imageButton.setImageResource(R.mipmap.baseline_exposure_zero_black_18dp)
            itemView.isClickable = false


            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.board, parent, false)
        )

    override fun getItemCount(): Int = dataSet.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.onBind()
    }
}




